﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace TreasureHunt
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameWorld : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        static GameWorld instance;
        public float deltaTime;
        List<GameObject> gameObjectList = new List<GameObject>();

        Rectangle playerRectangle;
        Player player;

        GameObject gameObject = new GameObject();
        private Texture2D backgroundTexture;
        //private Texture2D backgroundTexture2;
        //private Texture2D backgroundTexture3;
        //private Texture2D backgroundTexture4;
        //private Texture2D backgroundTexture5;
        //private Texture2D backgroundTexture6;
        //private Rectangle backgroundRectangle;


        BackgroundSprite mSprite;
        BackgroundSprite mBackgroundOne;
        BackgroundSprite mBackgroundTwo;
        BackgroundSprite mBackgroundThree;
        BackgroundSprite mBackgroundFour;
        BackgroundSprite mBackgroundFive;
        BackgroundSprite mBackgroundSix;

        //Skærm parameter
        int screenWidth;


        public static GameWorld Instance //implementering af singleton
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameWorld();
                }
                return instance;
            }
        }
        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            GameObject go = new GameObject();
            //Fremkaldes af player
            go.AddComponet(new SpriteRenderer(go, "characterFullSheetV4", 0, 1)); //Tilføjer billed via navn, hvilket lag den skal have og scalering den skal have
            go.AddComponet(new Animator(go));
            go.AddComponet(new Transform(go, Vector2.Zero));
            go.AddComponet(new Player(go));
            go.transform.Position = new Vector2(150, 390);
            gameObjectList.Add(go);


            mSprite = new BackgroundSprite();

            mBackgroundOne = new BackgroundSprite();
            mBackgroundOne.Scale = 1.0f;

            mBackgroundTwo = new BackgroundSprite();
            mBackgroundTwo.Scale = 1.0f;

            mBackgroundThree = new BackgroundSprite();
            mBackgroundThree.Scale = 1.0f;

            mBackgroundFour = new BackgroundSprite();
            mBackgroundFour.Scale = 1.0f;

            mBackgroundFive = new BackgroundSprite();
            mBackgroundFive.Scale = 1.0f;
            mBackgroundSix = new BackgroundSprite();
            mBackgroundSix.Scale = 1.0f;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);


            // TODO: use this.Content to load your game content here
            // mSpriteTexture = this.Content.Load<Texture2D>("militaWarrior");
            foreach (GameObject go in gameObjectList)
            {
                go.LoadContent(Content);
            }

            screenWidth = GraphicsDevice.Viewport.Width;

            //Tilføjelse af baggrunds lager
            backgroundTexture = Content.Load<Texture2D>("TransparentBG");


            //backgroundTexture2 = Content.Load<Texture2D>("bgLayer2");
            //backgroundTexture3 = Content.Load<Texture2D>("bgLayer3");
            //backgroundTexture4 = Content.Load<Texture2D>("bgLayer4");
            //backgroundTexture5 = Content.Load<Texture2D>("bgLayer5");
            //backgroundTexture6 = Content.Load<Texture2D>("bgLayer6");
            //backgroundRectangle = new Rectangle(0, 0, backgroundTexture.Width, backgroundTexture.Height);



            mBackgroundOne.LoadContent(this.Content, "FullBG1");
            mBackgroundOne.Position = new Vector2(0, 0);

            mBackgroundTwo.LoadContent(this.Content, "FullBG2");
            mBackgroundTwo.Position = new Vector2(mBackgroundOne.Position.X + mBackgroundOne.Size.Width, 0);

            mBackgroundThree.LoadContent(this.Content, "FullBG3");
            mBackgroundThree.Position = new Vector2(mBackgroundTwo.Position.X + mBackgroundTwo.Size.Width, 0);

            mBackgroundFour.LoadContent(this.Content, "FullBG4");
            mBackgroundFour.Position = new Vector2(mBackgroundThree.Position.X + mBackgroundThree.Size.Width, 0);

            mBackgroundFive.LoadContent(this.Content, "FullBG5");
            mBackgroundFive.Position = new Vector2(mBackgroundFour.Position.X + mBackgroundFour.Size.Width, 0);

            mBackgroundSix.LoadContent(this.Content, "FullBG2");
            mBackgroundSix.Position = new Vector2(mBackgroundFive.Position.X + mBackgroundFive.Size.Width, 0);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            // TODO: Add your update logic here
            foreach (GameObject go in gameObjectList)
            {
                go.Update();
            }


            if (mBackgroundOne.Position.X < -mBackgroundOne.Size.Width)
            {
                mBackgroundOne.Position.X = mBackgroundSix.Position.X + mBackgroundSix.Size.Width;
            }

            if (mBackgroundTwo.Position.X < -mBackgroundTwo.Size.Width)
            {
                mBackgroundTwo.Position.X = mBackgroundOne.Position.X + mBackgroundOne.Size.Width;
            }

            if (mBackgroundThree.Position.X < -mBackgroundThree.Size.Width)
            {
                mBackgroundThree.Position.X = mBackgroundTwo.Position.X + mBackgroundTwo.Size.Width;
            }

            if (mBackgroundFour.Position.X < -mBackgroundFour.Size.Width)
            {
                mBackgroundFour.Position.X = mBackgroundThree.Position.X + mBackgroundThree.Size.Width;
            }

            if (mBackgroundFive.Position.X < -mBackgroundFive.Size.Width)
            {
               mBackgroundFive.Position.X = mBackgroundFour.Position.X + mBackgroundFour.Size.Width;
            }

            if (mBackgroundSix.Position.X < -mBackgroundSix.Size.Width)
            {
                mBackgroundSix.Position.X = mBackgroundFive.Position.X + mBackgroundFive.Size.Width;
            }

            Vector2 aDirection = new Vector2(-2, 0);
            Vector2 aSpeed = new Vector2(100, 0);


            KeyboardState keyState = Keyboard.GetState();


            if (keyState.IsKeyDown(Keys.D))
            {
                mBackgroundOne.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                mBackgroundTwo.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                mBackgroundThree.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                mBackgroundFour.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                mBackgroundFive.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                mBackgroundSix.Position += aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            //if (keyState.IsKeyDown(Keys.A))
            //{
            //    mBackgroundOne.Position -= aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    mBackgroundTwo.Position -= aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    mBackgroundThree.Position -= aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    mBackgroundFour.Position -= aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //    mBackgroundFive.Position -= aDirection * aSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            //}

            if (playerRectangle.X <= 0) //Skulle gerne få spilleren låst inde i skærm området, men virker ikke
            {
                playerRectangle.X = 0;
                if (playerRectangle.X + backgroundTexture.Width >= screenWidth)
                    playerRectangle.X = screenWidth - backgroundTexture.Width;

            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.YellowGreen);

            spriteBatch.Begin();
            // TODO: Add your drawing code here


            //// spriteBatch.Draw(mSpriteTexture, mPostition, Color.White);
            spriteBatch.Draw(backgroundTexture, playerRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0); //Fremkalder det bagerst lag af baggrunde
                                                                                                                             //spriteBatch.Draw(backgroundTexture2, backgroundRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0);
                                                                                                                             //spriteBatch.Draw(backgroundTexture3, backgroundRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0);
                                                                                                                             //spriteBatch.Draw(backgroundTexture4, backgroundRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0);
                                                                                                                             //spriteBatch.Draw(backgroundTexture5, backgroundRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0);

            mBackgroundOne.Draw(this.spriteBatch);
            mBackgroundTwo.Draw(this.spriteBatch);
            mBackgroundThree.Draw(this.spriteBatch);
            mBackgroundFour.Draw(this.spriteBatch);
            mBackgroundFive.Draw(this.spriteBatch);
            mBackgroundSix.Draw(this.spriteBatch);
            // mSprite.Draw(this.spriteBatch);

            foreach (GameObject go in gameObjectList) //Fremkalder spilleren

            {
                go.Draw(spriteBatch);
            }


            //spriteBatch.Draw(backgroundTexture6, backgroundRectangle, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1); //Fremkalder det forreste baggrundsbillede

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
