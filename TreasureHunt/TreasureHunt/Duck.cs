﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreasureHunt
{
    class Duck : Interfaces.IStrategy
    {
        Animator animator;
        public Duck(Animator animator)
        {
            this.animator = animator;
        }

        public void Execute(ref DIRECTION currentDirection)
        {
            Vector2 translation = Vector2.Zero;
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.A))
            {
                currentDirection = DIRECTION.Left;
                animator.PlayAnimations("Down" + currentDirection);

            }
            if (keyState.IsKeyDown(Keys.D))
            {
                currentDirection = DIRECTION.Right;
                animator.PlayAnimations("Down" + currentDirection);

            }
            animator.PlayAnimations("Down" + currentDirection);
        }
    }
}

