﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreasureHunt
{
    class Walk : Interfaces.IStrategy
    {
        Animator animator;
        Transform transform;
        GameObject gameObject;
        float speed;
        public Walk(Transform transform, Animator animator, GameObject gameObject, float speed)
        {
            this.transform = transform;
            this.animator = animator;
            this.gameObject = gameObject;
            this.speed = speed;
        }

        public void Execute(ref DIRECTION currentDirection)
        {
            Vector2 translation = Vector2.Zero;

            KeyboardState keyState = Keyboard.GetState();

            /*if (keyState.IsKeyDown(Keys.W))
            {
                currentDirection = DIRECTION.Back;
                animator.PlayAnimations("WalkBack");
                translation += new Vector2(0, -1);
            }
            if (keyState.IsKeyDown(Keys.S))
            {
                currentDirection = DIRECTION.Front;
                animator.PlayAnimations("WalkFront");
                translation += new Vector2(0, 1);
            }*/
            if (keyState.IsKeyDown(Keys.A))
            {
                currentDirection = DIRECTION.Left;
                animator.PlayAnimations("WalkLeft");
                translation += new Vector2(-1, 0);
            }
            if (keyState.IsKeyDown(Keys.D))
            {
                currentDirection = DIRECTION.Right;
                animator.PlayAnimations("WalkRight");
                translation += new Vector2(1, 0);


            }
            gameObject.GetTransform.Translate(translation * GameWorld.Instance.deltaTime * speed);

        }
    }
}
