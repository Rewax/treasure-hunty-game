﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreasureHunt
{
    enum DIRECTION {Left, Right, Down, Up};

    class Player : Component, IUpdate, Interfaces.IAnimateable, ILoad
    {
        private float speed = 0;
        bool canMove = true;
        Interfaces.IStrategy strategy;
        DIRECTION currentDirection;
        Animator animator;

        public Player(GameObject gameObject) : base (gameObject)
        {

        }
        public void Update()
        {
            Vector2 translation = Vector2.Zero;

            KeyboardState keyState = Keyboard.GetState();
            if (canMove)
            {
                if ( keyState.IsKeyDown(Keys.A) || keyState.IsKeyDown(Keys.D))
                {
                    if (!(strategy is Walk))
                    {
                        strategy = new Walk(gameObject.transform, animator, gameObject, speed);
                    }
                }
                else
                {
                    strategy = new Idle(animator);
                }
                if (keyState.IsKeyDown(Keys.Space))
                {
                    strategy = new Attack(animator);

                    canMove = false;
                }
                //if (keyState.IsKeyDown(Keys.W))
                //{
                //    strategy = new UpAttack(animator);
                //}
                if (keyState.IsKeyDown(Keys.S))
                {
                    strategy = new Duck(animator);
                 
                }

            }
            strategy.Execute(ref currentDirection);

        }

        public void OnAnimationDone(string animationName)
        {
            if (animationName.Contains("Walk") || animationName.Contains("Attack"))
            {
                canMove = true;
            }
        }
        public void CreatAnimation()
        {


            animator.CreateAnimation("IdleLeft", new Animation(8, 576, 0, 64, 64, 10, new Vector2(0,0)));
            animator.CreateAnimation("IdleRight", new Animation(8, 128, 0, 64, 64, 10, new Vector2(0, 0)));

            animator.CreateAnimation("WalkLeft", new Animation(8, 576, 0, 64, 64, 10, new Vector2(0, 0)));
            animator.CreateAnimation("WalkRight", new Animation(3, 384, 0, 64, 64, 10, new Vector2(0,0)));

            animator.CreateAnimation("AttackRight", new Animation(4, 256, 0, 64, 64, 6, new Vector2(0, 0)));
            animator.CreateAnimation("AttackLeft", new Animation(4, 704, 0, 64, 64, 6, new Vector2(0, 0)));

            animator.CreateAnimation("DownRight", new Animation(3, 64, 0, 64, 64, 10, new Vector2(0, 0)));
            animator.CreateAnimation("DownLeft", new Animation(3, 512, 0, 64, 64, 4, new Vector2(0, 0)));

            //animator.CreateAnimation("UpRight", new Animation(5, 320, 0, 64, 64, 10, new Vector2(0, 0)));
            //animator.CreateAnimation("UpLeft", new Animation(5, 768, 0, 64, 64, 4, new Vector2(0, 0)));

            //animator.CreateAnimation("DieLeft", new Animation(3, 1070, 0, 150, 150, 5, Vector2.Zero));
            //animator.CreateAnimation("DieRight", new Animation(3, 1070, 3, 150, 150, 5, Vector2.Zero));

        }
        public void LoadContent(ContentManager content)
        {
            animator = (Animator)gameObject.GetComponets("Animator");
            CreatAnimation();
            animator.PlayAnimations("IdleRight");
        }
    }
}
